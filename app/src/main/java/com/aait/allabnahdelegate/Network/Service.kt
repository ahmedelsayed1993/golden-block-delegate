package com.aait.allabnahdelegate.Network

import com.aait.allabnahdelegate.Models.*
import okhttp3.MultipartBody
import retrofit2.Call
import retrofit2.http.Multipart
import retrofit2.http.POST
import retrofit2.http.Part
import retrofit2.http.Query

interface Service {
    @POST("about")
    fun About(@Query("lang") lang:String):Call<TermsResponse>

    @POST("terms")
    fun Terms(@Query("lang") lang: String):Call<TermsResponse>
    @POST("terms-sale")
    fun TermsSale(@Query("lang") lang: String):Call<TermsResponse>

    @POST("contact-us")
    fun ContactUs(@Query("lang") lang: String,
                  @Query("name") name:String,
                  @Query("phone") phone:String,
                  @Query("email") email:String,
                  @Query("message") message:String):Call<TermsResponse>

    @POST("follow-us")
    fun FollowUs():Call<SocialResponse>

    @POST("questions")
    fun Questions(@Query("lang") lang: String):Call<QuestionResponse>

    @POST("main-delegate")
    fun Main(@Query("lang") lang: String,
             @Query("delegate_id") delegate_id:Int):Call<OrderResponse>

    @POST("details-order-delegate")
    fun OrderDetails(@Query("lang") lang:String,
                     @Query("order_id") order_id:Int,
                     @Query("delegate_id") delegate_id:Int):Call<OrderDetailsResponse>

    @POST("details-order-delegate")
    fun Accept(@Query("lang") lang:String,
                     @Query("order_id") order_id:Int,
                     @Query("delegate_id") delegate_id:Int,
                     @Query("action") action:String,
                     @Query("confirm") confirm:Int):Call<BaseResponse>
    @Multipart
    @POST("sign-up-delegate")
    fun SignUp(@Query("phone") phone:String,
               @Query("name") name:String,
               @Query("email") email:String,
               @Query("address") address:String,
               @Query("lat") lat:String,
               @Query("lng") lng:String,
               @Query("id_number") id_number:String,
               @Part driving_license:MultipartBody.Part,
               @Query("password") password:String,
               @Query("device_id") device_id:String,
               @Query("device_type") device_type:String):Call<UserResponse>
    @POST("check-code")
    fun CheckCode(@Query("user_id") user_id:Int,
                  @Query("code") code:String,
                  @Query("lang") lang:String):Call<UserResponse>
    @POST("resend-code")
    fun ReSend(@Query("user_id") user_id:Int):Call<TermsResponse>

    @POST("sign-in")
    fun Login(@Query("lang") lang: String,
              @Query("phone") phone:String,
              @Query("password") password:String,
              @Query("device_id") device_id:String,
              @Query("device_type") device_type:String):Call<UserResponse>

    @POST("delegate-orders")
    fun Orders(@Query("lang") lang: String,
               @Query("delegate_id") delegate_id:Int,
               @Query("status") status:String):Call<OrderResponse>

    @POST("edit-profile")
    fun getProfile(@Query("user_id") user_id: Int):Call<UserResponse>
    @Multipart
    @POST("edit-profile")
    fun UpdateImage(@Query("user_id") user_id:Int,
                    @Query("lang") lang: String,
                    @Part avater:MultipartBody.Part):Call<UserResponse>

    @POST("edit-profile")
    fun update(@Query("user_id") user_id:Int,
               @Query("lang") lang: String,
               @Query("name") name:String,
               @Query("address") address:String,
               @Query("lat") lat:String,
               @Query("lng") lng: String,
               @Query("phone") phone:String,
               @Query("email") email: String):Call<UserResponse>
    @POST("reset-password")
    fun resetPassword(@Query("lang") lang:String,
                      @Query("user_id") user_id: Int,
                      @Query("current_password") current_password:String,
                      @Query("password") password:String):Call<BaseResponse>

    @POST("contact-whatsapp")
    fun WhatsApp():Call<TermsResponse>
}