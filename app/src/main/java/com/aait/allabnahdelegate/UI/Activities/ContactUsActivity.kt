package com.aait.allabnahdelegate.UI.Activities

import android.widget.Button
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import com.aait.allabnahdelegate.Base.Parent_Activity
import com.aait.allabnahdelegate.Client
import com.aait.allabnahdelegate.Models.TermsResponse
import com.aait.allabnahdelegate.Network.Service
import com.aait.allabnahdelegate.R
import com.aait.allabnahdelegate.Uitls.CommonUtil
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class ContactUsActivity:Parent_Activity() {
    override val layoutResource: Int
        get() = R.layout.activity_contact_us
    lateinit var back:ImageView
    lateinit var title:TextView
    lateinit var name:EditText
    lateinit var email:EditText
    lateinit var phone:EditText
    lateinit var message:EditText
    lateinit var send:Button

    override fun initializeComponents() {
        back = findViewById(R.id.back)
        title = findViewById(R.id.title)
        name = findViewById(R.id.name)
        email = findViewById(R.id.email)
        phone = findViewById(R.id.phone)
        message = findViewById(R.id.message)
        send = findViewById(R.id.send)
        back.setOnClickListener { onBackPressed() }
        title.text = getString(R.string.call_us)
        send.setOnClickListener {
            if (CommonUtil.checkEditError(name,getString(R.string.enter_name))||
                    CommonUtil.checkEditError(email,getString(R.string.enter_email))||
                    !CommonUtil.isEmailValid(email,getString(R.string.correct_email))||
                    CommonUtil.checkEditError(phone,getString(R.string.enter_phone))||
                    CommonUtil.checkEditError(message,getString(R.string.write_message))){
                return@setOnClickListener
            }else{
              SendContact()
            }
        }

    }

    fun SendContact(){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.ContactUs(mLanguagePrefManager.appLanguage,name.text.toString(),phone.text.toString(),
            email.text.toString(),message.text.toString())?.enqueue(object: Callback<TermsResponse>{
            override fun onFailure(call: Call<TermsResponse>, t: Throwable) {
                CommonUtil.handleException(mContext,t)
                t.printStackTrace()
                hideProgressDialog()
            }

            override fun onResponse(call: Call<TermsResponse>, response: Response<TermsResponse>) {
                hideProgressDialog()
                if (response.isSuccessful){
                    if (response.body()?.value.equals("1")){
                        CommonUtil.makeToast(mContext,response.body()?.data!!)
                        onBackPressed()
                    }else{
                        CommonUtil.makeToast(mContext,response.body()?.msg!!)
                    }
                }
            }
        })

    }
}