package com.aait.allabnahdelegate.UI.Fragments

import android.Manifest
import android.app.Activity
import android.content.Intent
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.view.View
import android.widget.LinearLayout
import android.widget.TextView
import androidx.core.app.ActivityCompat
import com.aait.allabnahdelegate.Base.BaseFragment
import com.aait.allabnahdelegate.Client
import com.aait.allabnahdelegate.Models.TermsResponse
import com.aait.allabnahdelegate.Network.Service
import com.aait.allabnahdelegate.R
import com.aait.allabnahdelegate.UI.Activities.*
import com.aait.allabnahdelegate.Uitls.CommonUtil
import com.aait.allabnahdelegate.Uitls.PermissionUtils
import com.bumptech.glide.Glide
import com.google.android.gms.common.internal.service.Common
import de.hdodenhof.circleimageview.CircleImageView
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class AccountFragment:BaseFragment() {
    override val layoutResource: Int
        get() = R.layout.fragment_my_account
    companion object {
        fun newInstance(): AccountFragment {
            val args = Bundle()
            val fragment = AccountFragment()
            fragment.setArguments(args)
            return fragment
        }
    }
    lateinit var about_app:LinearLayout
    lateinit var profile:LinearLayout
    lateinit var Privacy_policy:LinearLayout
    lateinit var Frequently_asked_questions:LinearLayout
    lateinit var terms_of_sale:LinearLayout
    lateinit var whats:LinearLayout
    lateinit var call_us:LinearLayout
    lateinit var follow_us:LinearLayout
    lateinit var share_app:LinearLayout
    lateinit var logout:LinearLayout
    lateinit var image:CircleImageView
    lateinit var name:TextView
    lateinit var email:TextView
    var phone = ""
    override fun initializeComponents(view: View) {
        about_app = view.findViewById(R.id.about_app)
        profile = view.findViewById(R.id.profile)
        Privacy_policy = view.findViewById(R.id.Privacy_policy)
        Frequently_asked_questions = view.findViewById(R.id.Frequently_asked_questions)
        terms_of_sale = view.findViewById(R.id.terms_of_sale)
        whats = view.findViewById(R.id.whats)
        call_us = view.findViewById(R.id.call_us)
        follow_us = view.findViewById(R.id.follow_us)
        share_app = view.findViewById(R.id.share_app)
        logout = view.findViewById(R.id.logout)
        about_app.setOnClickListener { startActivity(Intent(activity,AboutAppActivity::class.java)) }
        Privacy_policy.setOnClickListener { startActivity(Intent(activity,PrivacyPolicyActivity::class.java)) }
        terms_of_sale.setOnClickListener { startActivity(Intent(activity,TermsOfSaleActivity::class.java)) }
        share_app.setOnClickListener { mContext?.let { it1 -> CommonUtil.ShareApp(it1) } }
        call_us.setOnClickListener { startActivity(Intent(activity,ContactUsActivity::class.java)) }
        follow_us.setOnClickListener { startActivity(Intent(activity,FollowUsActivity::class.java)) }
        Frequently_asked_questions.setOnClickListener { startActivity(Intent(activity,QuestionsActivity::class.java)) }
        profile.setOnClickListener { startActivity(Intent(activity,ProfileActivity::class.java)) }
        image = view.findViewById(R.id.image)
        name = view.findViewById(R.id.name)
        email = view.findViewById(R.id.email)
        Glide.with(mContext!!).load(mSharedPrefManager.userData.avatar).into(image)
        name.text = mSharedPrefManager.userData.name
        email.text = mSharedPrefManager.userData.email
        getData()
        whats.setOnClickListener { getLocationWithPermission(phone) }


    }
    internal fun callnumber(number: String) {
        val intent = Intent(Intent.ACTION_DIAL)
        intent.data = Uri.parse("tel:$number")
        mContext!!.startActivity(intent)
    }

    fun getLocationWithPermission(number: String) {
        if (PermissionUtils.canMakeSmores(Build.VERSION_CODES.LOLLIPOP_MR1)) {
            if (!PermissionUtils.hasPermissions(mContext, Manifest.permission.CALL_PHONE)) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
                    ActivityCompat.requestPermissions(
                        mContext as Activity, PermissionUtils.CALL_PHONE,
                        300
                    )
            } else {
                callnumber(number)
            }
        } else {
            callnumber(number)
        }

    }

    fun getData(){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.WhatsApp()?.enqueue(object :
            Callback<TermsResponse> {
            override fun onFailure(call: Call<TermsResponse>, t: Throwable) {
                CommonUtil.handleException(mContext!!,t)
                t.printStackTrace()
                hideProgressDialog()
            }

            override fun onResponse(call: Call<TermsResponse>, response: Response<TermsResponse>) {
                hideProgressDialog()
                if (response.isSuccessful){
                    if (response.body()?.value.equals("1")){
                        phone = response.body()?.data!!
                    }else{
                        CommonUtil.makeToast(mContext!!,response.body()?.msg!!)
                    }
                }
            }

        })
    }
}