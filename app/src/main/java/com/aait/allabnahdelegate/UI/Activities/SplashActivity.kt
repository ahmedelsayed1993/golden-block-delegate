package com.aait.allabnahdelegate.UI.Activities

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.view.animation.AnimationUtils
import android.widget.ImageView
import com.aait.allabnahdelegate.Base.Parent_Activity
import com.aait.allabnahdelegate.R

class SplashActivity : Parent_Activity() {
    override val layoutResource: Int
        get() = R.layout.activity_splash
    var isSplashFinishid = false

    lateinit var logo : ImageView
    override fun initializeComponents() {
        logo = findViewById(R.id.image)


        val logoAnimation2 = AnimationUtils.loadAnimation(this, R.anim.anim_shine)

        Handler().postDelayed({
            logo.startAnimation(logoAnimation2)
            Handler().postDelayed({
                isSplashFinishid=true
                if (mSharedPrefManager.loginStatus==true){

                    startActivity(Intent(this@SplashActivity,MainActivity::class.java))
                    this@SplashActivity.finish()

                }else {

                    var intent = Intent(this@SplashActivity, ChooseLangActivity::class.java)
                    startActivity(intent)
                    finish()
                }

            }, 2100)
        }, 1800)
    }


}



