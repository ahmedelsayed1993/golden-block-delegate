package com.aait.allabnahdelegate.UI.Activities

import android.content.Intent
import android.text.Editable
import android.text.TextWatcher
import android.widget.Button
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import com.aait.allabnahdelegate.Base.Parent_Activity
import com.aait.allabnahdelegate.Client
import com.aait.allabnahdelegate.Models.TermsResponse
import com.aait.allabnahdelegate.Models.UserModel
import com.aait.allabnahdelegate.Models.UserResponse
import com.aait.allabnahdelegate.Network.Service
import com.aait.allabnahdelegate.R
import com.aait.allabnahdelegate.Uitls.CommonUtil
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class ActivateAccountActivity:Parent_Activity() {
    override val layoutResource: Int
        get() = R.layout.activity_activate_account
    lateinit var back:ImageView
    lateinit var one:EditText
    lateinit var two:EditText
    lateinit var three:EditText
    lateinit var four:EditText
    lateinit var next:Button
    lateinit var resend:TextView
    lateinit var userModel: UserModel
    override fun initializeComponents() {
        userModel = intent.getSerializableExtra("user") as UserModel
        back = findViewById(R.id.back)
        one = findViewById(R.id.one)
        two = findViewById(R.id.two)
        three = findViewById(R.id.three)
        four = findViewById(R.id.four)
        next = findViewById(R.id.next)
        resend = findViewById(R.id.resend)
        one.setSelection(0)
        two.setSelection(0)
        three.setSelection(0)
        four.setSelection(0)
        one.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(p0: Editable?) {
                two.requestFocus()
            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }

        })
        two.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(p0: Editable?) {
                three.requestFocus()
            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }

        })
        three.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(p0: Editable?) {
                four.requestFocus()
            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }

        })
        next.setOnClickListener {
            if (CommonUtil.checkEditError(one,getString(R.string.activation_code))||
                CommonUtil.checkEditError(two,getString(R.string.activation_code))||
                CommonUtil.checkEditError(three,getString(R.string.activation_code))||
                CommonUtil.checkEditError(four,getString(R.string.activation_code))){
                return@setOnClickListener
            }else{
                var code = one.text.toString()+two.text.toString()+three.text.toString()+four.text.toString()
                Activate(code)

            }
        }
        resend.setOnClickListener { Resend() }

    }

    fun Activate(code:String){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.CheckCode(userModel.id!!,code,mLanguagePrefManager.appLanguage)?.enqueue(object :
            Callback<UserResponse> {
            override fun onFailure(call: Call<UserResponse>, t: Throwable) {
                hideProgressDialog()
                CommonUtil.handleException(mContext,t)
                t.printStackTrace()
            }

            override fun onResponse(call: Call<UserResponse>, response: Response<UserResponse>) {
                hideProgressDialog()
                if (response.isSuccessful){
                    if (response.body()?.value.equals("1")){
                        startActivity(Intent(this@ActivateAccountActivity, LoginActivity::class.java))
                        this@ActivateAccountActivity.finish()
                    }else{
                        CommonUtil.makeToast(mContext,response.body()?.msg!!)
                    }
                }
            }
        })
    }

    fun Resend(){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.ReSend(userModel.id!!)?.enqueue(object :
            Callback<TermsResponse> {
            override fun onFailure(call: Call<TermsResponse>, t: Throwable) {
                CommonUtil.handleException(mContext,t)
                t.printStackTrace()
                hideProgressDialog()
            }

            override fun onResponse(
                call: Call<TermsResponse>,
                response: Response<TermsResponse>
            ) {
                hideProgressDialog()
                if (response.isSuccessful){
                    if (response.body()?.value.equals("1")){
                        CommonUtil.makeToast(mContext,response.body()?.msg!!)
                    }else{
                        CommonUtil.makeToast(mContext,response.body()?.msg!!)
                    }
                }
            }

        })
    }
}