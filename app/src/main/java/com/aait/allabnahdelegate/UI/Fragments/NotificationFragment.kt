package com.aait.allabnahdelegate.UI.Fragments

import android.os.Bundle
import android.view.View
import com.aait.allabnahdelegate.Base.BaseFragment
import com.aait.allabnahdelegate.R

class NotificationFragment:BaseFragment() {
    override val layoutResource: Int
        get() = R.layout.fragment_notification
    companion object {
        fun newInstance(): NotificationFragment {
            val args = Bundle()
            val fragment = NotificationFragment()
            fragment.setArguments(args)
            return fragment
        }
    }
    override fun initializeComponents(view: View) {

    }
}