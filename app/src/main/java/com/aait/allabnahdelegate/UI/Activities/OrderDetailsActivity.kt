package com.aait.allabnahdelegate.UI.Activities

import android.Manifest
import android.content.DialogInterface
import android.content.Intent
import android.location.Address
import android.location.Geocoder
import android.net.Uri
import android.os.Build
import android.provider.Settings
import android.util.Log
import android.widget.*
import androidx.appcompat.app.AlertDialog
import com.aait.allabnahdelegate.Base.Parent_Activity
import com.aait.allabnahdelegate.Client
import com.aait.allabnahdelegate.GPS.GPSTracker
import com.aait.allabnahdelegate.GPS.GpsTrakerListener
import com.aait.allabnahdelegate.Models.BaseResponse
import com.aait.allabnahdelegate.Models.OrderDetailsModel
import com.aait.allabnahdelegate.Models.OrderDetailsResponse
import com.aait.allabnahdelegate.Network.Service
import com.aait.allabnahdelegate.R
import com.aait.allabnahdelegate.Uitls.CommonUtil
import com.aait.allabnahdelegate.Uitls.DialogUtil
import com.aait.allabnahdelegate.Uitls.PermissionUtils
import com.bumptech.glide.Glide
import com.google.firebase.database.core.view.View
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.IOException
import java.util.*

class OrderDetailsActivity :Parent_Activity(),GpsTrakerListener{
    override fun onTrackerSuccess(lat: Double?, log: Double?) {
        if (startTracker) {
            if (lat != 0.0 && log != 0.0) {
                hideProgressDialog()
                // Log.e("lat",lat.toString()+" "+log.toString())
            }
        }
    }

    override fun onStartTracker() {
        startTracker = true
    }

    override val layoutResource: Int
        get() = R.layout.activity_order_details
    internal lateinit var geocoder: Geocoder
    internal lateinit var gps: GPSTracker
    internal var startTracker = false
    var mLang = ""
    var mLat = ""
    var result = ""
    private var mAlertDialog: AlertDialog? = null
    lateinit var title:TextView
    lateinit var back:ImageView
    lateinit var name:TextView
    lateinit var description:TextView
    lateinit var code:TextView
    lateinit var order_num:TextView
    lateinit var count_of_total:TextView
    lateinit var black_num:TextView
    lateinit var price_before_vit:TextView
    lateinit var image:ImageView
    lateinit var buyer_name:TextView
    lateinit var phone_number:TextView
    lateinit var city:TextView
    lateinit var street:TextView
    lateinit var region:TextView
    lateinit var location_on_map:TextView
    lateinit var delivery_before_vit:TextView
    lateinit var total_amount_before_vit:TextView
    lateinit var added_tax:TextView
    lateinit var total_amount_after_tax:TextView
    lateinit var accept:Button
    lateinit var refuse:Button
    lateinit var done:Button
    lateinit var lay:LinearLayout
    var id = 0
    var lat = ""
    var lng = ""
    override fun initializeComponents() {
        id = intent.getIntExtra("id",0)

        title = findViewById(R.id.title)
        back = findViewById(R.id.back)
        name = findViewById(R.id.name)
        description = findViewById(R.id.description)
        code = findViewById(R.id.code)
        order_num = findViewById(R.id.order_num)
        count_of_total = findViewById(R.id.count_of_total)
        black_num = findViewById(R.id.black_num)
        price_before_vit = findViewById(R.id.price_before_vit)
        image = findViewById(R.id.image)
        buyer_name = findViewById(R.id.buyer_name)
        phone_number = findViewById(R.id.phone_number)
        city = findViewById(R.id.city)
        street = findViewById(R.id.street)
        region = findViewById(R.id.region)
        location_on_map = findViewById(R.id.location_on_map)
        delivery_before_vit = findViewById(R.id.delivery_before_vit)
        total_amount_before_vit = findViewById(R.id.total_amount_before_vit)
        added_tax = findViewById(R.id.added_tax)
        total_amount_after_tax = findViewById(R.id.total_amount_after_tax)
        accept = findViewById(R.id.accept)
        refuse = findViewById(R.id.refuse)
        title.text = getString(R.string.order_details)
        lay = findViewById(R.id.lay)
        done = findViewById(R.id.done)
        back.setOnClickListener { onBackPressed()
        finish()}
        getLocationWithPermission()
        getData()
        accept.setOnClickListener { AcceptOrder() }
        refuse.setOnClickListener { RefuseOrder() }
        location_on_map.setOnClickListener {  startActivity(
            Intent(
                Intent.ACTION_VIEW,
                Uri.parse("http://maps.google.com/maps?saddr=" + mLat + "," + mLang + "&daddr=" + lat + "," + lng)
            )
        ) }

    }
    fun SetData(orderDetailsModel: OrderDetailsModel){
        name.text = orderDetailsModel.name
        description.text = orderDetailsModel.description
        code.text = getString(R.string.code)+" "+orderDetailsModel.code + "#"
        order_num.text = orderDetailsModel.order_id.toString()
        count_of_total.text = orderDetailsModel.count
        price_before_vit.text = orderDetailsModel.total+getString(R.string.rs)
        Glide.with(mContext).load(orderDetailsModel.image).into(image)
        buyer_name.text = orderDetailsModel.username
        phone_number.text = orderDetailsModel.phone
        city.text = orderDetailsModel.city
        street.text = orderDetailsModel.street
        region.text = orderDetailsModel.region
        location_on_map.text = orderDetailsModel.address
        delivery_before_vit.text = orderDetailsModel.delivery+getString(R.string.rs)
        total_amount_before_vit.text = orderDetailsModel.total+getString(R.string.rs)
        added_tax.text = orderDetailsModel.total_tax+getString(R.string.rs)
        lat = orderDetailsModel.lat!!
        lng = orderDetailsModel.lng!!
        total_amount_after_tax.text = orderDetailsModel.total_amount.toString()+getString(R.string.rs)
        if (orderDetailsModel.status.equals("current")){
            lay.visibility = android.view.View.VISIBLE
            done.visibility = android.view.View.GONE
        }else if (orderDetailsModel.status.equals("accept")){
            lay.visibility = android.view.View.GONE
            done.visibility = android.view.View.VISIBLE
        }
        done.setOnClickListener { CompleteOrder() }

    }

    fun getData(){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.OrderDetails(mLanguagePrefManager.appLanguage,id,mSharedPrefManager.userData.id!!)
            ?.enqueue(object : Callback<OrderDetailsResponse> {
                override fun onFailure(call: Call<OrderDetailsResponse>, t: Throwable) {
                    CommonUtil.handleException(mContext,t)
                    t.printStackTrace()
                    hideProgressDialog()
                }

                override fun onResponse(
                    call: Call<OrderDetailsResponse>,
                    response: Response<OrderDetailsResponse>
                ) {
                    hideProgressDialog()
                    if (response.isSuccessful){
                        if (response.body()?.value.equals("1")){
                            SetData(response.body()?.data!!)
                        }else{
                            CommonUtil.makeToast(mContext,response.body()?.msg!!)
                        }
                    }
                }
            })
    }
    fun AcceptOrder(){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.Accept(mLanguagePrefManager.appLanguage,id,mSharedPrefManager.userData.id!!,"accept",1)
            ?.enqueue(object :Callback<BaseResponse>{
                override fun onFailure(call: Call<BaseResponse>, t: Throwable) {
                    CommonUtil.handleException(mContext,t)
                    t.printStackTrace()
                    hideProgressDialog()
                }

                override fun onResponse(
                    call: Call<BaseResponse>,
                    response: Response<BaseResponse>
                ) {
                    hideProgressDialog()
                    if (response.isSuccessful){
                        if (response.body()?.value.equals("1")){
                            startActivity(Intent(this@OrderDetailsActivity,BackToMainActivity::class.java))
                            finish()
                        }else{
                            CommonUtil.makeToast(mContext,response.body()?.msg!!)
                        }
                    }
                }
            })
    }
    fun RefuseOrder(){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.Accept(mLanguagePrefManager.appLanguage,id,mSharedPrefManager.userData.id!!,"refuse",1)
            ?.enqueue(object :Callback<BaseResponse>{
                override fun onFailure(call: Call<BaseResponse>, t: Throwable) {
                    CommonUtil.handleException(mContext,t)
                    t.printStackTrace()
                    hideProgressDialog()
                }

                override fun onResponse(
                    call: Call<BaseResponse>,
                    response: Response<BaseResponse>
                ) {
                    hideProgressDialog()
                    if (response.isSuccessful){
                        if (response.body()?.value.equals("1")){
                            startActivity(Intent(this@OrderDetailsActivity,MainActivity::class.java))
                            finish()
                        }else{
                            CommonUtil.makeToast(mContext,response.body()?.msg!!)
                        }
                    }
                }
            })
    }
    fun CompleteOrder(){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.Accept(mLanguagePrefManager.appLanguage,id,mSharedPrefManager.userData.id!!,"completed",1)
            ?.enqueue(object :Callback<BaseResponse>{
                override fun onFailure(call: Call<BaseResponse>, t: Throwable) {
                    CommonUtil.handleException(mContext,t)
                    t.printStackTrace()
                    hideProgressDialog()
                }

                override fun onResponse(
                    call: Call<BaseResponse>,
                    response: Response<BaseResponse>
                ) {
                    hideProgressDialog()
                    if (response.isSuccessful){
                        if (response.body()?.value.equals("1")){
                            startActivity(Intent(this@OrderDetailsActivity,MainActivity::class.java))
                            finish()
                        }else{
                            CommonUtil.makeToast(mContext,response.body()?.msg!!)
                        }
                    }
                }
            })
    }

    fun getLocationWithPermission() {
        gps = GPSTracker(mContext, this)
        if (PermissionUtils.canMakeSmores(Build.VERSION_CODES.LOLLIPOP_MR1)) {
            if (!(PermissionUtils.hasPermissions(mContext, Manifest.permission.ACCESS_FINE_LOCATION)&&
                        (PermissionUtils.hasPermissions(mContext,
                            Manifest.permission.ACCESS_COARSE_LOCATION)))) {
                CommonUtil.PrintLogE("Permission not granted")
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    requestPermissions(
                        PermissionUtils.GPS_PERMISSION,
                        800
                    )
                    Log.e("GPS", "1")
                }
            } else {
                getCurrentLocation()
                Log.e("GPS", "2")
            }
        } else {
            Log.e("GPS", "3")
            getCurrentLocation()
        }

    }

    internal fun getCurrentLocation() {
        gps.getLocation()
        if (!gps.canGetLocation()) {
            mAlertDialog = DialogUtil.showAlertDialog(mContext,
                getString(R.string.gps_detecting),
                DialogInterface.OnClickListener { dialogInterface, i ->
                    mAlertDialog?.dismiss()
                    val intent = Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS)
                    startActivityForResult(intent, 300)
                })
        } else {
            if (gps.getLatitude() !== 0.0 && gps.getLongitude() !== 0.0) {
                mLat = gps.getLatitude().toString()
                mLang = gps.getLongitude().toString()
                val addresses: List<Address>
                geocoder = Geocoder(this, Locale.getDefault())
                try {
                    addresses = geocoder.getFromLocation(
                        java.lang.Double.parseDouble(mLat),
                        java.lang.Double.parseDouble(mLang),
                        1
                    )
                    if (addresses.isEmpty()) {
                        Toast.makeText(
                            mContext,
                            resources.getString(R.string.detect_location),
                            Toast.LENGTH_SHORT
                        ).show()
                    } else {
                        result = addresses[0].getAddressLine(0)
                        Log.e("address",result)





                    }


                } catch (e: IOException) {
                }



            }
        }
    }
}