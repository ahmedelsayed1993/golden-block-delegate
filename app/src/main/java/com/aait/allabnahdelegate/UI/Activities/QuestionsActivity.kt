package com.aait.allabnahdelegate.UI.Activities

import android.util.Log
import android.view.View
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.aait.allabnahdelegate.Base.Parent_Activity
import com.aait.allabnahdelegate.Client
import com.aait.allabnahdelegate.Listeners.OnItemClickListener
import com.aait.allabnahdelegate.Models.QuestionModel
import com.aait.allabnahdelegate.Models.QuestionResponse
import com.aait.allabnahdelegate.Network.Service
import com.aait.allabnahdelegate.R
import com.aait.allabnahdelegate.UI.Adapters.QuestionsAdapter
import com.aait.allabnahdelegate.Uitls.CommonUtil
import com.google.gson.Gson
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class QuestionsActivity :Parent_Activity(),OnItemClickListener{
    override fun onItemClick(view: View, position: Int) {

    }

    override val layoutResource: Int
        get() = R.layout.activity_questions

    lateinit var title:TextView
    lateinit var back:ImageView
    lateinit var rv_recycle: RecyclerView
    internal var layNoInternet: RelativeLayout? = null

    internal var layNoItem: RelativeLayout? = null

    internal var tvNoContent: TextView? = null

    var swipeRefresh: SwipeRefreshLayout? = null
    lateinit var linearLayoutManager: LinearLayoutManager
    var questionsModels=ArrayList<QuestionModel>()
    lateinit var questionsAdapter: QuestionsAdapter
    override fun initializeComponents() {
        title = findViewById(R.id.title)
        back = findViewById(R.id.back)
        rv_recycle = findViewById(R.id.rv_recycle)
        layNoInternet = findViewById(R.id.lay_no_internet)
        layNoItem = findViewById(R.id.lay_no_item)
        tvNoContent = findViewById(R.id.tv_no_content)
        swipeRefresh = findViewById(R.id.swipe_refresh)
        title.text = getString(R.string.Frequently_asked_questions)
        back.setOnClickListener { onBackPressed() }
        linearLayoutManager = LinearLayoutManager(mContext,LinearLayoutManager.VERTICAL,false)
        questionsAdapter = QuestionsAdapter(mContext,questionsModels,R.layout.recycler_question)
        questionsAdapter.setOnItemClickListener(this)
        rv_recycle.layoutManager = linearLayoutManager
        rv_recycle.adapter = questionsAdapter
        swipeRefresh!!.setColorSchemeResources(
            R.color.colorPrimary,
            R.color.colorPrimaryDark,
            R.color.colorAccent
        )
        swipeRefresh!!.setOnRefreshListener {
            getData()

        }
        getData()

    }
    fun getData(){
        showProgressDialog(getString(R.string.please_wait))
        layNoInternet!!.visibility = View.GONE
        layNoItem!!.visibility = View.GONE
        Client.getClient()?.create(Service::class.java)?.Questions(mLanguagePrefManager.appLanguage)?.enqueue(object :
            Callback<QuestionResponse> {
            override fun onFailure(call: Call<QuestionResponse>, t: Throwable) {
                CommonUtil.handleException(mContext,t)
                t.printStackTrace()
                layNoInternet!!.visibility = View.VISIBLE
                layNoItem!!.visibility = View.GONE
                swipeRefresh!!.isRefreshing = false
                hideProgressDialog()
            }

            override fun onResponse(
                call: Call<QuestionResponse>,
                response: Response<QuestionResponse>
            ) {
                swipeRefresh!!.isRefreshing = false
                hideProgressDialog()
                if (response.isSuccessful) {
                    if (response.body()?.value.equals("1")) {
                        Log.e("myJobs", Gson().toJson(response.body()!!.data))
                        if (response.body()!!.data?.isEmpty()!!) {
                            layNoItem!!.visibility = View.VISIBLE
                            layNoInternet!!.visibility = View.GONE
                            tvNoContent!!.setText(R.string.content_not_found_you_can_still_search_the_app_freely)

                        } else {
//
                            questionsAdapter.updateAll(response.body()!!.data!!)
                        }
//
                    }else {
                        CommonUtil.makeToast(mContext,response.body()?.msg!!)
                    }

                }
            }
        })
    }
}