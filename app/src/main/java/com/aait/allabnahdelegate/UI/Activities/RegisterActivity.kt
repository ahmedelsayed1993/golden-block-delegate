package com.aait.allabnahdelegate.UI.Activities

import android.content.Intent
import android.net.Uri
import android.os.Build
import android.text.InputType
import android.util.Log
import android.widget.*
import com.aait.allabnahdelegate.Base.Parent_Activity
import com.aait.allabnahdelegate.Client
import com.aait.allabnahdelegate.Models.UserResponse
import com.aait.allabnahdelegate.Network.Service
import com.aait.allabnahdelegate.R
import com.aait.allabnahdelegate.Uitls.CommonUtil
import com.aait.allabnahdelegate.Uitls.PermissionUtils
import com.fxn.pix.Options
import com.fxn.pix.Pix
import com.fxn.utility.ImageQuality
import com.google.gson.Gson
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.File

class RegisterActivity:Parent_Activity() {
    override val layoutResource: Int
        get() = R.layout.activity_register
    lateinit var phone:EditText
    lateinit var name:EditText
    lateinit var email:EditText
    lateinit var address:TextView
    lateinit var id_num:EditText
    lateinit var driving:TextView
    lateinit var password:EditText
    lateinit var view:ImageView
    lateinit var confirm_password:EditText
    lateinit var view1:ImageView
    lateinit var forgot_pass:CheckBox
    lateinit var register:Button
    lateinit var login:LinearLayout
    var lat = ""
    var lng = ""
    var location = ""
    internal var returnValue: java.util.ArrayList<String>? = java.util.ArrayList()
    internal var options = Options.init()
        .setRequestCode(100)                                                 //Request code for activity results
        .setCount(1)                                                         //Number of images to restict selection count
        .setFrontfacing(false)                                                //Front Facing camera on start
        .setImageQuality(ImageQuality.HIGH)                                  //Image Quality
        .setPreSelectedUrls(returnValue)                                     //Pre selected Image Urls
        .setScreenOrientation(Options.SCREEN_ORIENTATION_PORTRAIT)           //Orientaion
        .setPath("/pix/images")
    private var ImageBasePath: String? = null

    override fun initializeComponents() {
        phone = findViewById(R.id.phone)
        name = findViewById(R.id.name)
        email = findViewById(R.id.email)
        address = findViewById(R.id.address)
        id_num = findViewById(R.id.id_num)
        driving = findViewById(R.id.driving)
        password = findViewById(R.id.password)
        view = findViewById(R.id.view)
        confirm_password = findViewById(R.id.confirm_password)
        view1 = findViewById(R.id.view1)
        forgot_pass = findViewById(R.id.forgot_pass)
        register = findViewById(R.id.register)
        login = findViewById(R.id.login)
        login.setOnClickListener { startActivity(Intent(this,LoginActivity::class.java)) }
        address.setOnClickListener { startActivityForResult(Intent(this@RegisterActivity,LocationActivity::class.java),1) }
        view.setOnClickListener {  if (password.inputType== InputType.TYPE_TEXT_VARIATION_NORMAL){
            password.inputType = InputType.TYPE_CLASS_TEXT or InputType.TYPE_TEXT_VARIATION_PASSWORD

        }
        else{
            password.inputType = InputType.TYPE_TEXT_VARIATION_NORMAL
        } }
        view1.setOnClickListener {
            if (confirm_password.inputType== InputType.TYPE_TEXT_VARIATION_NORMAL){
                confirm_password.inputType = InputType.TYPE_CLASS_TEXT or InputType.TYPE_TEXT_VARIATION_PASSWORD

            }
            else{
                confirm_password.inputType = InputType.TYPE_TEXT_VARIATION_NORMAL
            }
        }

        driving.setOnClickListener {
            if (PermissionUtils.canMakeSmores(Build.VERSION_CODES.LOLLIPOP_MR1)) {
                if (!(PermissionUtils.hasPermissions(mContext,
                        android.Manifest.permission.CAMERA)&& PermissionUtils.hasPermissions(mContext,
                        android.Manifest.permission.READ_EXTERNAL_STORAGE)&& PermissionUtils.hasPermissions(mContext,
                        android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
                            )) {
                    CommonUtil.PrintLogE("Permission not granted")
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        requestPermissions(
                            PermissionUtils.IMAGE_PERMISSIONS,
                            400
                        )
                    }
                } else {
                    Pix.start(this, options)
                    CommonUtil.PrintLogE("Permission is granted before")
                }
            } else {
                CommonUtil.PrintLogE("SDK minimum than 23")
                Pix.start(this, options)
            }
        }

        register.setOnClickListener {
            if (CommonUtil.checkEditError(phone,getString(R.string.phone_number))||
                    CommonUtil.checkLength1(phone,getString(R.string.phone_length),10)||
                    CommonUtil.checkEditError(name,getString(R.string.enter_name))||
                    CommonUtil.checkEditError(email,getString(R.string.enter_email))||
                    !CommonUtil.isEmailValid(email,getString(R.string.correct_email))||
                    CommonUtil.checkTextError(address,getString(R.string.enter_address))||
                    CommonUtil.checkEditError(id_num,getString(R.string.enter_id_number))||
                    CommonUtil.checkTextError(driving,getString(R.string.enter_driving_licences))||
                    CommonUtil.checkEditError(password,getString(R.string.enter_password))||
                    CommonUtil.checkLength(password,getString(R.string.password_length),6)||
                    CommonUtil.checkEditError(confirm_password,getString(R.string.enter_password))){
                return@setOnClickListener
            }else{
                if (!password.text.toString().equals(confirm_password.text.toString())){
                    CommonUtil.makeToast(mContext,getString(R.string.password_not_match))
                }else{
                    if (!forgot_pass.isChecked){
                        CommonUtil.makeToast(mContext,getString(R.string.raed_accept))
                    }else{

                        Register(ImageBasePath!!)
                    }
                }
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == 100) {
            returnValue = data!!.getStringArrayListExtra(Pix.IMAGE_RESULTS)

            ImageBasePath = returnValue!![0]


            if (ImageBasePath != null) {

                driving.text = getString(R.string.image_attached)
            }
        }else {
            if (data?.getStringExtra("result") != null) {
                location = data?.getStringExtra("result").toString()
                lat = data?.getStringExtra("lat").toString()
                lng = data?.getStringExtra("lng").toString()
                address.text = location
            } else {
                location = ""
                lat = ""
                lng = ""
                address.text = ""
            }
        }
    }

    fun Register(path:String){
        showProgressDialog(getString(R.string.please_wait))
        var filePart: MultipartBody.Part? = null
        val ImageFile = File(path)
        val fileBody = RequestBody.create(MediaType.parse("*/*"), ImageFile)
        filePart = MultipartBody.Part.createFormData("driving_license", ImageFile.name, fileBody)
        Client.getClient()?.create(Service::class.java)?.SignUp(phone.text.toString(),name.text.toString(),email.text.toString()
        ,address.text.toString(),lat,lng,id_num.text.toString(),filePart,password.text.toString(),"yyhjjkjhhvbjdjkgkmjwjfj","android")
            ?.enqueue(object : Callback<UserResponse> {
                override fun onFailure(call: Call<UserResponse>, t: Throwable) {
                    CommonUtil.handleException(mContext,t)
                    t.printStackTrace()
                    hideProgressDialog()
                    Log.e("error", Gson().toJson(t))
                }

                override fun onResponse(
                    call: Call<UserResponse>,
                    response: Response<UserResponse>
                ) {
                    hideProgressDialog()
                    if (response.isSuccessful){
                        if (response.body()?.value.equals("1")){
                            val intent = Intent(this@RegisterActivity,ActivateAccountActivity::class.java)
                            intent.putExtra("user",response.body()?.data)
                            startActivity(intent)
                            finish()
                        }else{
                            CommonUtil.makeToast(mContext,response.body()?.msg!!)
                        }
                    }
                }
            })
    }
}