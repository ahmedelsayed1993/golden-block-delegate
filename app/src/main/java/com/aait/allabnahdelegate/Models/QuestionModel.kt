package com.aait.allabnahdelegate.Models

import java.io.Serializable

class QuestionModel:Serializable {
    var id:Int?=null
    var question:String?=null
    var answer:String?=null
}