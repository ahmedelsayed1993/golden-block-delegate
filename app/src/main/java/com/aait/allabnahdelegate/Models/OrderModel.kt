package com.aait.allabnahdelegate.Models

import java.io.Serializable

class OrderModel:Serializable {
    var id:Int?=null
    var image:String?=null
    var name:String?=null
    var description:String?=null
}