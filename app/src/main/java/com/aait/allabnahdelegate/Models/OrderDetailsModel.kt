package com.aait.allabnahdelegate.Models

import java.io.Serializable

class OrderDetailsModel:Serializable {
    var image:String?=null
    var name:String?=null
    var description:String?=null
    var code:Int?=null
    var order_id:Int?=null
    var count:String?=null
    var total:String?= null
    var color:String?=null
    var status:String?=null
    var username:String?=null
    var phone:String?=null
    var city:String?=null
    var region:String?=null
    var street:String?=null
    var lat:String?=null
    var lng:String?=null
    var address:String?=null
    var delivery:String?=null
    var tax:String?=null
    var total_tax:String?=null
    var total_amount:Double?=null

}